###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "datacenter_name" {
  description = "(Required)[string] The name of the Virtual Data Center."
  default     = "mgmt"
}

variable "datacenter_location" {
  description = "(Required)[string] one of de/fra, de/txl, us/las, us/ewr or gb/lhr"
  default     = "de/fra"
}

variable "k8s_cluster_name" {
  description = "(Required)[string] The name of the Kubernetes Cluster."
  default     = "mgmt_clusterapi"
}

variable "k8s_node_pool_name" {
  description = "(Required)[string] The name of the Kubernetes Node Pool."
  default     = "mgmt_clusterapi_node_pool"
}

variable "k8s_version" {
  description = "(Required)[string] The desired Kubernetes Version."
  default     = "1.19.10"
}

variable "public_lan" {
  description = "(Required)[bool] Indicates if the LAN faces the public Internet (true) or not (false). Need to be (true)"
  default     = "true"
}
