# clusterapi-hcloud_mgmt-ionos

This repository creates a Managed Kubernetes Cluster on IONOS Cloud used as a MGMT Cluster for ClusterAPI.

Furthermore its bootstrapping a CusterAPI cluster on Hetzner Cloud.

## Requirements

- clusterctl
- just
- kubectl
- helm
- ionosctl
- terraform

## Prerequisites

[Documentation](https://github.com/cluster-api-provider-hcloud/cluster-api-provider-hcloud)

We need to register the __hcloud__ infrastructure provider in `$HOME/.cluster-api/clusterctl.yaml`:

```yaml
providers:
  - name: "hcloud"
    url: "https://github.com/cluster-api-provider-hcloud/cluster-api-provider-hcloud/releases/latest/infrastructure-components.yaml"
    type: "InfrastructureProvider"
```

## Workflow

- `just init` Initializes IONOS managed Kubernets mgmt cluster
- `just init` Initializes clusterctl on the mgmt cluster (includes mgmtconfig)
- `just config` Applies ClusterResourceSets & generates `cluster.yaml` from the provided Helm-Chart
- `just cluster-create` Creates the CRDs at mgmt cluster and starts deploying the cluster to hcloud (includes init, prepare & config)
- `just mgmtconfig` Get kubeconfig from the mgmt cluster saved to `./mgmtconfig`
- `just kubeconfig` Get hcloud kubeconfig saved to `./kubeconfig`
- `just clean` Remove the cluster
- `just clusterctl-clean` Resets the clusterctl installation on the MGMT cluster
- `just destroy` Remove the cluster & Terrafrom

## Time estimation

| Task | Time |
| ---- | ---- |
| IONOS Management Cluster | ~15-20min
| Hetzner CluserAPI Cluster | ~10-15min
| Overall| ~25-35min
