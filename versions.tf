terraform {
  required_providers {
    ionoscloud = {
      source  = "ionos-cloud/ionoscloud"
      version = "5.0.4"
    }
  }
  required_version = ">= 0.13"
}
