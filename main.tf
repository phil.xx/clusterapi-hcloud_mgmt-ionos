module "datacenter" {
  source              = "git@gitlab.com:phil.xx/terraform-modules.git//ionos//datacenter?ref=main"
  datacenter_name     = var.datacenter_name
  datacenter_location = var.datacenter_location
}

module "k8s_cluster" {
  source      = "git@gitlab.com:phil.xx/terraform-modules.git//ionos//k8s_cluster?ref=main"
  name        = var.k8s_cluster_name
  k8s_version = var.k8s_version
}

module "lan" {
  source        = "git@gitlab.com:phil.xx/terraform-modules.git//ionos//lan?ref=main"
  name          = var.k8s_cluster_name
  public        = var.public_lan
  datacenter_id = module.datacenter.datacenter_id
}

resource "ionoscloud_k8s_node_pool" "k8s" {
  depends_on  = [module.lan]
  name        = var.k8s_node_pool_name
  k8s_version = var.k8s_version
  auto_scaling {
    min_node_count = 1
    max_node_count = 3
  }
  #lans              = [module.lan.lan_id]
  datacenter_id     = module.datacenter.datacenter_id
  k8s_cluster_id    = module.k8s_cluster.cluster_id
  cpu_family        = "INTEL_SKYLAKE"
  availability_zone = "AUTO"
  storage_type      = "SSD"
  node_count        = 3
  cores_count       = 2
  ram_size          = 2048
  storage_size      = 40
  #public_ips        = [ "85.184.251.100", "157.97.106.15", "157.97.106.25" ]
}
