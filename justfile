init:
	#!/usr/bin/env sh
	source .envrc

	terraform init && terraform apply -auto-approve

prepare: mgmtconfig
	#!/usr/bin/env sh
	source .envrc

	export EXP_CLUSTER_RESOURCE_SET=true && export KUBECONFIG=./mgmtconfig

	if [ "$(kubectl get namespaces ${CLUSTERNAME} -o jsonpath='{.metadata.name}')" == "${CLUSTERNAME}" ]; then \
		echo -e "\033[35;1mNamespace ${CLUSTERNAME} already created.Nothing to do.\033[0m"; \
	else kubectl create namespace ${CLUSTERNAME} ; \
	fi

	if [ "$(kubectl --namespace ${CLUSTERNAME} get secret hetzner-token -o jsonpath='{.metadata.name}')" == "hetzner-token" ]; then \
		echo -e "\033[35;1mSecret hetzner-token already created.Nothing to do.\033[0m";
	else kubectl --namespace=$CLUSTERNAME create secret generic hetzner-token --from-literal=token=$HCLOUD_TOKEN ; \
	fi

	if [ "$(kubectl --namespace capi-hcloud-system get providers infrastructure-hcloud -o jsonpath='{.metadata.name}')" == "infrastructure-hcloud" ]; then \
	    echo -e "\033[35;1mProvider infrastructure-hcloud is already initialized.\033[0m"; \
	else clusterctl init --infrastructure hcloud:v0.1.4; \
	fi

config:
	#!/usr/bin/env sh
	source .envrc

	export KUBECONFIG=./mgmtconfig

	kubectl --namespace=$CLUSTERNAME apply -f ./ClusterResourceSets

	mkdir $CLUSTERNAME

	helm template ./charts/clusterapi-hcloud \
	--set cluster.name=$CLUSTERNAME \
	--set cluster.namespace=$CLUSTERNAME \
	-f ./charts/clusterapi-hcloud/values.yaml \
	-f ./charts/clusterapi-hcloud/defaults/values.yaml \
	> ./$CLUSTERNAME/cluster.yaml

mgmtconfig:
	#!/usr/bin/env sh
	source .envrc

	ionosctl k8s kubeconfig get --cluster-id=$(terraform state pull |jq -r '.resources[] | select(.module == "module.k8s_cluster") | .instances[0].attributes.id') > ./mgmtconfig

kubeconfig:
	#!/usr/bin/env sh
	source .envrc

	export KUBECONFIG=./mgmtconfig

	kubectl --namespace=$CLUSTERNAME get secret $CLUSTERNAME-kubeconfig \
	-o jsonpath={.data.value} | base64 --decode \
	> ./$CLUSTERNAME/kubeconfig

cluster-create: init prepare config
	#!/usr/bin/env sh
	source .envrc

	kubectl --namespace=$CLUSTERNAME --kubeconfig=./mgmtconfig apply -f ./$CLUSTERNAME/cluster.yaml

clean:
	#!/usr/bin/env sh
	source .envrc

	export KUBECONFIG=./mgmtconfig

	if [ "$(kubectl --namespace=$CLUSTERNAME get cluster $CLUSTERNAME --ignore-not-found -o jsonpath='{.metadata.name}')" == "${CLUSTERNAME}" ]; then \
	    kubectl --namespace=$CLUSTERNAME delete cluster $CLUSTERNAME;
	else echo -e "\033[35;1mCluster $CLUSTERNAME has already been delete.\033[0m"; \
	fi

clusterctl-clean: clean
	#!/usr/bin/env sh
	source .envrc

	export KUBECONFIG=./mgmtconfig

	clusterctl delete --all --include-crd --include-namespace

destroy: clean
	#!/usr/bin/env sh
	source .envrc

	terraform destroy



