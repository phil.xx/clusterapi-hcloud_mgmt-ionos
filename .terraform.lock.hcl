# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/ionos-cloud/ionoscloud" {
  version     = "5.0.4"
  constraints = "5.0.4"
  hashes = [
    "h1:OmpLKsaUNNdl2BAW1BHqXjNlr9B8ufn1v+yuNauFKCM=",
    "zh:0f5a7b959c51a73e5e2b536edde17cecaa01afacbab334deab6bfeeeefa9d426",
    "zh:130d70c98a298742f482df47fe6cb982692f7b175450803a41582502faab341e",
    "zh:6837735dab4b8cddedb25ba37ed55ad3627e160a8b3afced04814ab327202adf",
    "zh:94c7899db378fa06a6830f8f48bfbfaf93629c6f2a2c9df77619c657a915e309",
    "zh:9ec581adf8ecb37f4e6233ec926e47cab5699b8566d94ac5425c61ab8946c911",
    "zh:9ff95bc82ef5a5142b181722307e50976db613c9861ffa51c45941d9bdfa8c28",
    "zh:a8944b33032d321ef40fa3bd9ef1d16f26e7d799339ec5b3ac561aab1651d313",
    "zh:aabb03500e6dca3b549381b45773dadfb530577ee85179eba132bf8a08ec8fa6",
    "zh:c8063e098727c00a31ae72a9c9359540a160606a2c0542515dda8d2d89472a4a",
    "zh:ca7c54439ccba6b4051259b010c3372cc95889f6c79849dbaf4ed6546a1b0b09",
    "zh:f3cb401cfeb99d85a23ea757ede058f4ff4db71ab73d9cc3c2ccf77bf2f32d54",
  ]
}
